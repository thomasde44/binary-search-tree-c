#include <stdio.h>
#include <stdlib.h>
#include "bst.h"
// Input: ’size’: size of an array
// Output: a pointer of type BStree,
//         i.e. a pointer to an allocated memory of BStree_struct type
// Effect: dynamically allocate memory of type BStree_struct
//         allocate memory for a Node array of size+1 for member tree_nodes
//         allocate memory for an unsigned int array of size+1 for member free_nodes
//         set member size to ’size’;
//         set entry free_nodes[i] to i
//         set member top to 1;
//         set member root to 0;
BStree bstree_ini(int size) {
    BStree_struct* BStree = malloc(sizeof(BStree_struct));
    Node* treeNodes = malloc((size+1)*sizeof(Node));
    unsigned int* freeNodes = malloc((size+1)*sizeof(unsigned int));
    for(int i = 0; i < (size+1); i++){
        freeNodes[i] = i;
    }
    BStree->tree_nodes = treeNodes;
    BStree->size = size;
    BStree->top = 1;
    BStree->root = 0;
    BStree->free_nodes = freeNodes;
    return BStree;
}
// Input: ’bst’: a binary search tree
//        ’key’: a pointer to Key
//        ’data’: an integer
// Effect: ’data’ with ’key’ is inserted into ’bst’
//         if ’key’ is already in ’bst’, do nothing
static int root = 1;
void bstree_insert(BStree bst, Key *key, Data data){
    if(bst->top == 1){
        bst->root++;
        bst->tree_nodes[bst->top].key = key;
        bst->tree_nodes[bst->top].data = data;
        bst->tree_nodes[bst->top].left = 0;
        bst->tree_nodes[bst->top].right = 0;
        bst->top++;
    }
    else{
        if(key_comp(bst->tree_nodes[root].key, key) == 1){
            if(bst->tree_nodes[root].left == 0){
                bst->tree_nodes[root].left = bst->top;
                bst->tree_nodes[bst->top].key = key;
                bst->tree_nodes[bst->top].data = data;
                bst->tree_nodes[bst->top].left = 0;
                bst->tree_nodes[bst->top].right = 0;
                bst->top++;
            }
            else{
                root = bst->tree_nodes[root].left;
                bstree_insert(bst, key, data);
            }
        }
        else if(key_comp(bst->tree_nodes[root].key, key) == -1){
            if(bst->tree_nodes[root].right == 0){
                bst->tree_nodes[root].right = bst->top;
                bst->tree_nodes[bst->top].key = key;
                bst->tree_nodes[bst->top].data = data;
                bst->tree_nodes[bst->top].left = 0;
                bst->tree_nodes[bst->top].right = 0;
                bst->top++;
            }
            else{
                root = bst->tree_nodes[root].right;
                bstree_insert(bst, key, data);
            }
        }
        else{
            if(key_comp(bst->tree_nodes[root].key, key) == 0){
                //continue;
            }
        }
    }
    root = 1;
}
// Input: ’bst’: a binary search tree
// Effect: print all the nodes in bst using in order traversal
//static Key* min = NULL;
//static Node minNode;


void bstree_traversal(BStree bst) {
    int presenttime = bst->root;
    if(bst->root != 0){
        bst->root = bst->tree_nodes[bst->root].left;
        bstree_traversal(bst);

        bst->root = presenttime;
        print_node(bst->tree_nodes[bst->root]);

        bst->root = bst->tree_nodes[bst->root].right;
        bstree_traversal(bst);
        bst->root = presenttime;
    }
}
// Input: ’bst’: a binary search tree
// Effect: all dynamic memory used by bst are freed
void bstree_free(BStree bst) {

    for(int i = 0; i < bst->top; i++){
      memset(bst->tree_nodes, 0, sizeof(bst->tree_nodes[i]));
    }
    memset(bst, 0, sizeof(bst));
}